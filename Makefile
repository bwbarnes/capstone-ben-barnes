name = thesis

all: images pdf
	evince $(name).pdf

images:
	ln -s -f -r img/src/UML/*.png -t img

pdf: blbiliography
	pdflatex $(name)
	pdflatex $(name)

blbiliography:
	pdflatex $(name)
	bibtex $(name)

tidy:
	@rm -rf *.pdf *.aux *.bbl *.blg *.dvi *.lof *.log *.lot *.toc
	@rm -rf ./tex/*.aux

clean: tidy
	@rm -rf *.pdf
