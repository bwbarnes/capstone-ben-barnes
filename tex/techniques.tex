\chapter{Techniques}
\label{chapter:techniques}

\section{Experimentation}
One of the primary constraints for this project was the computing hardware. In developing a software package to implement the required functionality, it was necessary to make optimal use of the computing resources that were available. To that end, much work in the early stages of the project was focused on experimentation, to determine which techniques and algorithms would make efficient use of the Odroid hardware.

For the results of these experiments, please see chapter \ref{chapter:results}.

\subsection{Feature Detector Time Measurement}
The optical flow software contains two core operations which represent the majority of the computational work: feature detection and feature tracking. To ensure the efficient use of computing resources, it was necessary to compare the available algorithms and select one which offered the best compromise between execution time and feature quality.

The algorithms for testing were chosen from those available through OpenCV. The algorithms tested were:

\begin{itemize}
	\item Harris Corners
	\item Good Features To Track (GFTT)
	\item FAST
	\item AGAST
	\item AGAST (standalone)
\end{itemize}

The standalone AGAST implementation was provided by the authors of the original paper on AGAST\cite{mairEtAl2010}. It was standalone in the sense that it was not provided through OpenCV, and required manual integration into the OpenCV framework.

A simple test program was written to determine the relative performance of these algorithms. This program implemented the procedure detailed in figure \ref{fig:detectorTimeActivityDiagram}.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.55]{img/detectorTimeTest}
	\caption[Activity diagram, feature detector execution time test] {
		The procedure used to implement the feature detector algorithm timing test.
	}
	\label{fig:detectorTimeActivityDiagram}
\end{figure}

\FloatBarrier

To perform the timing measurements, the OpenCV timing infrastructure was used. This consists of a \textit{tick counter}, which stores the number of fixed periods since some epoch, and a \textit{tick frequency}, a measure indicating the number of ticks per second. To provide a measure of the elapsed time between two events, the tick count is recorded at the time of each event. The initial time is subtracted from the final time, and the result divided by the tick frequency.

\subsection{KLT Time Measurement}

Although no alternatives to the KLT algorithm were proposed, it was nevertheless valuable to gain an understanding of the time required to track different numbers of features. This measurement informed design choices later in the project.

As with the feature detection algorithms, a simple test program was written to provide this timing measurement for differing numbers of features. This program implemented the procedure outlined in figure \ref{fig:kltTimeActivityDiagram}.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.55]{img/kltTimeTest}
	\caption[Activity diagram, KLT execution time test] {
		The procedure used to implement the KLT timing test.
	}
	\label{fig:kltTimeActivityDiagram}
\end{figure}

\FloatBarrier

This procedure generated a data set which included the time taken to track each frame and the number of active features before tracking. Features may be lost during tracking, leading to a decreasing number of tracked features over time.

The data set was then sorted by the number of features before tracking, and the time for each iteration plotted against this number. The shape of the resultant plot was used to create an approximate model of tracking time versus feature count, which was used to determine the number of features to be tracked in the final application.

\subsection{Feature Lifetime Testing}
Alongside execution time, it was also desired to assess the quality of the features selected by each algorithm. Rather than defining a mathematical measure of feature quality, a simple comparative test was used.

For the purposes of this test, it was assumed that the rate of feature loss during tracking may be used as a proxy for a feature quality measure. If one feature could be tracked over a greater number of frames than another given the same video sequence, presumably the quality of the first feature was higher. To implement this test, a program was written to implement the procedure outlined in figure \ref{fig:featureLifetimeActivityDiagram}.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.55]{img/featureLifetimeTest}
	\caption[Activity diagram, feature lifetime test] {
		The procedure used to implement the feature lifetime test.
	}
	\label{fig:featureLifetimeActivityDiagram}
\end{figure}

\FloatBarrier

This process produced a set of data indicating the decline in number of features over the course of the video sequence. The gradient of the resulting plot indicated the feature loss rate. By plotting the results for each detection algorithm along the same axes, it was possible to gain insight into the different rates of feature loss among algorithms, which provided an indication of the quality of the features they detected.

\subsection{USB Latency Measurement}	
To help reason about the behaviour of the final system, it was necessary to determine the time interval between an image being captured by the camera and when it became available to the application. Measuring this delay was non-trivial; the lack of a synchronised time reference for both the Odroid and the Firefly camera meant that it was not possible to simply compare the time of an event as it appeared to both devices.

To take this measurement, it was decided to use the GPIO port of the Odroid to provide a shutter trigger signal to the Firefly. The Odroid's internal time reference could then be used for both events, removing the need to synchronise the clocks of both devices.

The standard method for accessing GPIO pins through Linux is through the userspace sysfs interface\cite{sysfs}. The sysfs interface allows a userspace program to request control of a GPIO pin from the kernel. If the kernel accepts this request, a node is created in \texttt{/sys/class/gpio} that allows a userspace program to control, for example, pin state and direction by writing to files.

The drawback of this method is that it can introduce a large and unknown latency. This decreases the value of the sysfs method for applications which require accurate timing.

The Odroid's Exynos 4412 processor implements memory-mapped GPIO, which provides a more direct alternative to sysfs. By writing to a particular memory location, a GPIO pin can be toggled as fast as the hardware allows. In the case of the Odroid, the limited set of GPIOs available are part of the GPX1 bank, which does not exhibit appreciable latency between a memory write and the corresponding change in pin state (this is in contrast with other GPIO banks that require around 800 clock cycles of an internal bus before changes take place).

Given that Linux implements virtual memory, it is not possible for a program to access a hardware memory location directly. This restriction can be circumvented through the use of the \texttt{mmap()} system call with \texttt{/dev/mem}. This technique allows a page of memory to be mapped into the virtual address space, with all offsets maintained relative to the page location. Then, the hardware GPIO memory can be directly operated on using only standard C/C++ operations.

The relevant registers for GPIO manipulation are listed in table \ref{tab:gpioRegisters}.

\begin{table}[h]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		Register & Description \\
		\hline
		GPX1CON & Used to set the input/output direction of the GPX1 pins\\
		GPX1DAT & Stores the current value (high or low) of the GPX1 pins\\
		GPX1PUD & Controls each GPX1 pin's pull-up/down resistors.\\
		GPX1DRV & Controls each GPX1 pin's drive strength\\
		\hline
	\end{tabular}
	\caption{Samsung Exynos 4412 GPIO control registers.}
	\label{tab:gpioRegisters}
\end{table}

To test the USB latency, a simple program was developed to send a trigger signal and record the time before a response. The method used by this program is detailed in figure \ref{fig:usbLatencyActivityDiagram}.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.55]{img/usbLatencyMeasurement}
	\caption[Activity diagram, USB latency test] {
		The procedure used to implement the USB latency test.
	}
	\label{fig:usbLatencyActivityDiagram}
\end{figure}

\FloatBarrier

To provide an image in response to the Odroid's GPIO signal edge, it was necessary to place the Firefly camera into external trigger mode. This mode initiates image capture on a specified signal edge on one of the camera's GPIO pins. The FlyCapture2 API\cite{flycap} allows a callback function to be triggered when the driver finishes receiving an image. This allows asynchronous operation, and lets the timing measurement program yield the processor using \texttt{sleep()}. This means a busy-wait is not necessary, and the timing measurement is not affected by waiting for the kernel to preempt the timing process. Inaccuracies may nevertheless be introduced if the system is operating under heavy load, if the callback function's thread priority is much lower than that of competing processes. To prevent this from affecting the results, the tests were run with the Odroid under very low load.

\section{Application Design}

The end result of this project was an on-board, offline, real-time system which demonstrates that the Odroid is capable of performing both feature tracking and visual-inertial fusion in real time.

Whilst an online application would have provided a more direct demonstration of this capability, it would also have required significant work towards hardware development and interfacing that would not have directly contributed to the goals of the project. Furthermore, an offline implementation is able to use a recorded data set, which allows the final state estimation to be compared against that provided by a non-real-time reference implementation. For these reasons, the final application structure has been designed to take a recorded data set as input rather than real-time data from sensors. However, where possible the application was modularised such that core sections may be reused in an online implementation.

Several factors influenced the design of the application. First, the application was designed to run in multiple threads. To simplify this, the Qt framework was used to provide a decoupled producer/consumer model of communication. This framework allows objects to emit data in the form of a \textit{signal}, with connected objects receiving it in a \textit{slot}. Neither object is required to maintain a reference to the other, which prevents strong coupling between the two. Furthermore, as signals can be queued, they are inherently thread-safe.

Second, the application was designed to be as modular as possible. Designing the application to closely model a block diagram allowed objects to be rearranged to adjust behaviour. This was done to allow code to e reused in an online implementation, to be developed in a future project.

Finally, the modelling of the application on a block diagram allowed intuitive reasoning about the system's behaviour.

\subsection{Top-Level Layout}
\label{sec:topLevelLayout}
For a diagram of the top-level application layout, see figure \ref{fig:trackerClassDiagram}.

\begin{figure}[h]
	\centering
	\rotatebox{90} {
		\includegraphics[scale=0.55]{img/topLevelLayout}
	}
	\caption[Class diagram, top-level layout] {
		A simplified top-level layout of the offline tracker application.
	}
	\label{fig:trackerClassDiagram}
\end{figure}

\FloatBarrier

The application works as follows:
\begin{enumerate}
	\item First, the CSVParser object reads a .csv file in to memory. This .csv file contains IMU data and frame numbers with timestamps. The CSVParser converts the .csv entries into numeric data structures, then passes these structures on to subsequent objects.
	\item The IMUPackager collects IMU packets until an image packet arrives, then sends the total collection to the scheduler. Packaging the data in this manner allows the scheduler to alternate between forwarding IMU collections and FeatureVectors, ensuring that all measurements are sent to the Kalman Filter in the correct order without having to explicitly compare timestamps.
	\item When the CSVParser emits an image packet (a timestamped frame number), the ImageReader object finds the image file with a matching frame number and loads it into an OpenCV matrix structure. This is then forwarded to the PyramidAdapter.
	\item The PyramidAdapter object creates an image pyramid from its input. From this point on, all images are stored as pyramids. As the tracker requires an image pyramid to operate, storing images as pyramids prevents having to explicitly convert each image twice - once for each pass through the tracker.
	\item The ImageDelay and ImageSwitch are an abstraction which allows the application to initialise itself in a controlled manner. If an image is available from the tracker, that is used in preference over the same image from the ImageDelay. Frames from the tracker are preferred as they have already been populated with features, so contain more useful information than raw images from the ImageDelay.
	\item Regardless of whether an image has been used previously, it is passed to the PointManager in the same manner. The PointManager detects as many additional features as are required to bring the total up to the level chosen for the application. If the image has not been initialised, this means that the PointManager extracts the total number of features in one operation. If the image has been previously populated with features, but has an insufficient number due to tracking errors, the PointManager will add only the required number of features to return the image to its full complement. If no additional features are required, the PointManager does nothing.
	\item Next, images are passed into the Tracker. The previous objects are arranged such that the Tracker is always presented with two consecutive images; the first image will include a set of features to be tracked, and the second will be empty. The Tracker uses the KLT algorithm to attempt to locate the first image's features in the second image. Once this is completed, the Tracker sends the second image via the PointManager to be returned as the first image for the next iteration.
	\item Finally, a pair of feature vectors is sent to the scheduler. The first vector represents the positions of the features in the first image, with the second vector similarly representing their positions in the second image. Each element in the first vector corresponds to an element in the second. The scheduler sends these feature vectors to the KalmanFilter, alternating with IMU measurements to ensure that data enters the filter in chronological order. For the purposes of this application, the KalmanFilter may be treated as a black box which provides position, velocity and attitude estimates by fusing the visual and inertial information from the application.
\end{enumerate}

\subsection{Multithreading}

As discussed in the previous section, the application was designed to be run in multiple threads to take advantage of the four processing cores available on the Odroid. The application was split such that each thread handled the responsibilities of one or more objects. The objects were separated as detailed in table \ref{tab:objectThreadAffiliations}.

\begin{table}[h]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		Thread & Objects\\
		\hline
		1 & CSVParser, IMUPackager\\
		2 & ImageReader, PyramidAdapter, FrameDelay\\
		3 & ImageSwitch, PointManager\\
		4 & Tracker\\
		5 & Scheduler, KalmanFilter\\
		\hline
	\end{tabular}
	\caption[Object thread affiliations]{Breakdown of the thread affiliations of the various application objects.}
	\label{tab:objectThreadAffiliations}
\end{table}

This structure was devised based on the relationships between objects. First of all, it was decided to place each of the most demanding objects - the Tracker, PointManager and KalmanFilter - into their own threads. This was to ensure that all three could operate simultaneously when required, whilst leaving one core to perform the remaining operations and other system tasks.

To determine where to place the less demanding objects, their patterns of communication were analysed. If it was beneficial for one object to be in synchronous communication with another, then the two were placed into the same thread. If some communication latency was acceptable, then the objects were placed in different threads. Consider, for example, the ImageReader and PyramidAdapter - placing these images into the same thread meant that as soon as an image was read, it was converted into its pyramid representation. This saved buffering images unnecessarily.

Finally, some objects were placed in different threads to allow the operating system to adjust their behaviour dynamically. For example, objects which required file I/O, such as the CSVReader and ImageReader, could potentially cause their threads to sleep after making an I/O request. This would then delay all other operations in that thread. Even though creating separate threads for I/O operations meant that the total number of threads was greater than the number of physical cores, the increased granularity gave the operating system more flexibility in scheduling them.

\subsection{Optimisation}
In addition to the multithreaded design, some additional techniques were used to improve the execution speed of the application. These included:

\begin{description}
	\item[OpenCV build options]\hfill\\
		The OpenCV libraries have been written to take advantage of pre-existing libraries and various forms of hardware acceleration available to modern computing platforms. Care was taken when compiling OpenCV to ensure that all available optimisations were activated. Such optimisations included support for NEON SIMD instructions, Eigen, OpenCL, and Intel's Thread Building Blocks.
	\item[Storing images as pyramids]\hfill\\
		Initially, images were passed to OpenCV's \texttt{calcOpticalFlowPyrLK()} function in their raw form, without first converting them to pyramids. The \texttt{calcOpticalFlowPyrLK()} would then perform this conversion internally. Given that each image passes through the tracking function twice - once as the first image, and once as the second - this meant that each image was undergoing the same conversion process twice. Performing this conversion explicitly, and storing the image in pyramid form, made a significant difference to the execution speed of the KLT algorithm.
\end{description}

\subsection{Kalman Filter}
The Kalman Filter was provided by Dinuka Abeywardena, and was not an outcome of this project. A simple API was provided to take visual and inertial measurements, then return a state vector describing several current state variables.