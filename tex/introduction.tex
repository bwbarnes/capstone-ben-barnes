\chapter{Introduction}

\section{Overview}
Semi-autonomous Micro Aerial Vehicles (MAVs), commonly referred to as `drones', are seeing increased adoption in industry to help solve challenges in remote inspection, mapping, videography and payload delivery. One MAV format which has seen a rapid rise in popularity is the quadrotor, or quadcopter. 

\begin{wrapfigure}{o}{0.5\textwidth}
	\centering
	\includegraphics[width=\linewidth]{img/aeryonScoutUAV}
	\caption[A commercial quadcopter platform] {
		A commercial quadcopter platform with a gimbal-mounted camera \citelongp{aeryonScout}.
	}
	\label{fig:hobbyistQuad}
\end{wrapfigure}

Quadcopters are simple to construct, maneuverable, and can be built from relatively low-cost components. The quadcopter consists of thin frame, generally shaped like an uppercase letter X or H, with motor and propeller assemblies at its four extremities. Batteries, radio receivers, controllers and the like are generally placed at the centre of the frame. For applications requiring video or image capture it is common to place the camera under the centre of the frame, often mounted on a stabilising gimbal. The gimbal may also be remote controlled, allowing an operator to control the orientation of the camera independently of the pose of the vehicle.

In use, the main benefit of the quadcopter is its maneuverability. The quadcopter can translate independently along all three spatial axes, and rotate independently around the \(z\) (yaw) axis. Rotation around the \(x\) and \(y\) (roll and pitch) axes results in translation, so these movements are not independent.

Whilst the quadcopter's geometry brings benefits in maneuverability, it has the drawback of being inherently unstable. Thus, closed-loop control systems are generally required if the vehicle is to be controlled by a human pilot. Additionally, pilots generally desire to control the vehicle in terms of orthogonal velocities (up/down, left/right, back/forth) and rotations (yaw). The controller is also responsible for mapping these orthogonal control inputs to the direct control inputs of the system, namely the rotational velocities of the four rotors.

With the control of a quadcopter presented in terms of translational velocities, ideally the vehicle would remain fixed in space when given a neutral control input. In practice, there are a number of factors which prevent this - the dynamic instability of the vehicle, sensitivity to disturbances such as wind, and the lack of a direct position measurement as feedback for the control system.

The lack of a direct position measurement has been addressed to some degree by the incorporation of GPS receivers into quadcopter platforms. GPS allows a quadrotor to know its position within a bounded error, so long as it is able to maintain reception of the GPS satellite signals. Whilst this prevents a large drift in position over long missions, GPS alone is insufficient to allow accurate position-keeping over short distances and timescales. This is due to its relatively high noise and low update rate. The GPS information is therefore combined, or \textit{fused}, with IMU data to achieve a position estimate which benefits from the strengths of both sensors.

As GPS is reliant on a line-of-sight path to the GPS satellite constellation, it becomes unusable in circumstances where this path is interrupted. This includes a number of environments where quadcopter usage may be desirable such as inside warehouses, in dense urban environments, under bridges (for structural inspection purposes), or inside mines. The lack of a suitable replacement for GPS in these circumstances severly limits the practical application of quadrotor aircraft.

Some solutions have been proposed to this problem, generally involving the use of a camera system and computer vision algorithms (see chapter \ref{chapter:litReview}). Of these solutions, many are constrained to particular quadcopter designs or environments. Those that are more general tend to involve a heavy computational workload, which means they are of limited practical use with current technology - whilst the algorithms could be implemented on a powerful off-board computer system, receiving sensor data and returning control inputs to the quadcopter via radio, the communication delays complicate the low-latency control needed to stabilise quadcopters in practice. To make such systems practical, they should be simple enough to be implemented on-board with low-power embedded computer systems. Recent developments in computer architecture and silicon photolithography processes have resulted in a great increase in the power of small, embedded computer systems; nevertheless, the bridge between computing capabilities and the demands of visual state estimation algorithms has yet to be fully closed.

\section{Problem Definition}

The problem to be solved, then, is how to implement a combined visual and inertial control system which can operate in real-time, on hardware compact enough to be situated on-board a small quadcopter. The control system should provide at least drift-free short-term position keeping, and minimise long-term drift in the position estimate for the vehicle. Such a control system would be of great benefit to semi-autonomous quadrotor operation in GPS-denied areas, as it would allow pilots with minimal training to maintain accurate positional control over a quadcopter when used for fine visual inspection, payload delivery and the like.

\section{Report Structure}
The remainder of this report is structured as follows:

\begin{description}
	\item[Chapter \ref{chapter:litReview}: Literature Review]\hfill\\
		This chapter presents a discussion of previous work in this field.
	\item[Chapter \ref{chapter:theory}: Theory]\hfill\\
		This chapter presents the theory behind the systems developed for this project.
	\item[Chapter \ref{chapter:equipment}: Equipment]\hfill\\
		This chapter introduces the hardware and software tools used throughout this project.
	\item[Chapter \ref{chapter:techniques}: Techniques]\hfill\\
		This chapter presents the implementation details of the project, including the preliminary experiments and the final application design.
	\item[Chapter \ref{chapter:results}: Results]\hfill\\
		This chapter presents and describes the results of the preliminary experiments and final application testing.
\end{description}