\chapter{Theory}
\label{chapter:theory}

\section{Introduction}
The following section presents some of the background theory related to this project. Section \ref{sec:quadArchitecture} discusses the design and architecture of quadcopter platforms in terms of hardware, sensors and control systems. Section \ref{sec:computerVision} introduces relevant material from the field of computer vision, including an explanation of `features' in a computer vision context and a discussion of some of the algorithms used in this work.

\section{Quadcopter Architecture}
\label{sec:quadArchitecture}
This section discusses the physical design of quadcopter MAVs, and the common control techniques used to aid in stabilising and localising them.

\subsection{Hardware}
The standard quadcopter is mechanically and conceptually simple. The frame of the vehicle is generally thin and lightweight, with four extremities on which rotor assemblies are mounted. The frame may take various shapes.

If the four rotors were to rotate in the same direction, this would apply a net torque to the vehicle and cause it to turn. To prevent this, the rotors are arranged into two pairs; one pair rotates clockwise, and the other counterclockwise. This requires two different rotor types with opposite pitch angles, such that all the thrust forces are applied in the same direction regardless of their direction of rotation.

For examples of quadrotor frame shapes and rotor pairings, see figure \ref{fig:layouts}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\linewidth]{img/layouts}
	\caption[Common quadcopter layouts] {
		Examples of four common quadrotor layouts.
	}
	\label{fig:layouts}
\end{figure}

For an understanding of how quadcopters effect motion in space, consider the `Plus' configuration shown in figure \ref{fig:layouts}. The grey arrow indicates the nominal `forward' direction for the vehicle, which we shall consider to lie along the positive \(x\) axis. The positive \(y\) axis extends to the left, with the positive \(z\) axis extending out of the page.

When stationary, the body of the quadcopter lies in the \(xy\) plane. In this position, the thrust vectors of the four rotors lie along the negative \(z\) axis. To move forwards, the quadcopter increases the thrust from the rear rotor and decreases that from the front rotor, which has the effect of tilting the vehicle around the \(y\) axis. This tilts the thrust vectors from the rotors, introducing a component along the \(x\) axis which pushes the vehicle forwards.

To move sideways, the thrust from the left and right rotors is adjusted to tilt the vehicle around the \(x\) axis, introducing a thrust component in the \(y\) direction.

To move along the \(z\) axis, the thrust of all four rotors is simply increased or decreased in unison.

To rotate the vehicle around the \(z\) axis, the rotation rates of the rotors are adjusted such that a net torque exists around the \(z\) axis whilst maintaining a constant net thrust in the negative \(z\) direction. This is achieved by decreasing the rotation rate of one rotor pair, say, the clockwise pair, whilst increasing the rotation rate of the other. As the faster-rotating pair now applies more torque to the frame, the vehicle rotates.

A simple model for the motion of a quadcopter can be represented using several equations of motion. First, we must define two three-dimensional Euclidean frames of reference. The earth frame is the frame from which a static observer would see the vehicle. The body frame is the frame local to the quadcopter. Using the standard basis, the body frame is arranged such that the forward direction is represented by \(\hat{x}\), the left direction is represented by \(\hat{y}\), and the upwards direction is represented by \(\hat{z}\) (all relative to the quadcopter). The earth frame is arranged such that when the quadcopter is located at the origin with zero rotation, the earth and body axes align.

Next, a relationship between rotor speed and thrust is required. In the body frame:

\begin{equation}
	\vec{F}_{T_{i}} = k_{T_{i}}\vec{\omega}_{i}
	\label{eqn:singleThrust}
\end{equation}

Where \(\vec{F}_{T_{i}}\) is the thrust force due to rotor \(i\), \(\vec{\omega}_{i}\) is the rotational velocity vector of rotor \(i\), and \(k_{T_{i}}\) is the thrust coefficient of rotor \(i\) in \(\si{\newton\second\per\radian}\).

The equation for the total thrust force acting on the quadcopter is therefore:

\begin{equation}
	\vec{F}_{T} = \sum_{i=1}^{4} k_{T_{i}}\vec{\omega}_{i}
	\label{eqn:totalThrust}
\end{equation}

The quadcopter can also develop torque independently around its three axes. The torque due to thrust, acting around the centre of mass, can be determined with the following equation:

\begin{equation}
	\vec{\tau}_{T} = \sum_{i=1}^{4} \vec{r}_{i}\times\vec{F}_{T_{i}}
\end{equation}

where \(\vec{\tau}_{T}\) is the combined torque vector around the \(x\) and \(y\) axes, \(\vec{r}_{i}\) is the position vector from the centre of mass to rotor \(i\), and \(\vec{F}_{T_{i}}\) is the thrust due to propeller \(i\) from equation \ref{eqn:singleThrust}.

The torque around the \(z\) axis can be likewise estimated with the following equation:

\begin{equation}
	\vec{\tau}_{z} = \sum_{i=1}^{4} k_{Q_{i}}\vec{\omega}_{i}
\end{equation}

Where \(\vec{\tau}_{z}\) is the torque around the \(z\) axis, \(k_{Q_{i}}\) is the torque coefficient in \(\si{\newton\meter\second\per\radian}\) for rotor \(i\) and \(\vec{\omega}_{i}\) is the angular velocity of rotor \(i\).

A common method for representing the orientation of aircraft is the use of yaw, pitch and roll angles. Yaw, or heading, describes rotation around the \(z\) axis in the earth frame. Pitch describes rotation around the \(y\) axis, or front-back tilt, in the body frame relative to the earth frame's horizontal plane. Roll describes rotation around the \(x\) axis, or side-to-side tilt, in the body frame relative to the earth frame's horizontal plane.

\begin{table}[h]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		Name & Rotation Axis & Symbol\\
		\hline
		Roll & \(x\) & \(\theta\)\\
		Pitch & \(y\) & \(\phi\)\\
		Yaw & \(z\) & \(\psi\)\\
		\hline
	\end{tabular}
	\caption[Rotation axes \& equivalent notation]{Equivalent notation for roll, pitch and yaw angles}
	\label{tab:orientationNotation}
\end{table}


A rotation matrix may be derived from these angles, which allows vectors in the body frame to be transformed into the earth frame.

\begin{equation}
	\mathbf{R} = 
	\begin{pmatrix}
		\cos(\phi)\cos(\psi) & -\sin(\psi) & -\sin(\phi)\\
		\sin(\psi) & \cos(\theta)\cos(\psi) & -\sin(\theta)\\
		\sin(\phi) & \sin(\theta) & \cos(\theta\cos(\phi)\\
	\end{pmatrix}
	\label{eqn:rotMatrix}
\end{equation}

This allows for the transformation of thrust forces and torque into the earth frame using the following relation:

\begin{equation}
	\vec{V}_{E} = \mathbf{R}\vec{V}_{B}
\end{equation}

Where \(\vec{V}_{E}\) and \(\vec{V}_{B}\) are \((x, y, z)^{T}\) column vectors and \(\mathbf{R}\) is the rotation matrix defined in \ref{eqn:rotMatrix}.

With the projection of force and torque into the earth frame comes the ability to create equations of motion in the earth frame. This allows for the analysis of motion, relative to a stationary observer, caused by the thrust forces of the quadcopter. For example, to find the quadcopter's acceleration vector in the earth frame:

\begin{equation}
	\vec{A}_{E} = \frac{1}{m}(\mathbf{R}\vec{F}_{B})
\end{equation}

where \(\vec{F}_{B}\) is the total thrust force and \(m\) is the mass of the vehicle.

Such equations allow the quadcopter to infer its motion in the earth frame given only a local acceleration vector and roll, pitch and yaw angles. Both of these measurements may be provided by MEMS accelerometers and gyroscopes as discussed in section \ref{sec:controlSystems}.

Given a measurement of its motion in the earth frame, this can then be used as an input ito a control system designed to maintain a set position, velocity or rotation in the earth frame. This is discussed further in section \ref{sec:controlSystems}.

\subsection{Control Systems}
\label{sec:controlSystems}
Several concepts from the study of control systems are used in the control and stabilisation of quadcopters. Though this is an area of active research, some concepts are in common enough use to warrant discussion here.

Within a single quadcopter there may be several PID controllers arranged in one or more layers. 
The first layer of controllers may be responsible for maintaining the yaw, pitch and roll angles of the vehicle. On top of, and encapsulating this layer may be controllers for velocity along the three translational axes. Finally, some quadcopters may also include controllers for position along the translational axes. This layer of control is generally only required when the quadcopter is required to navigate to waypoints or along paths autonomously.

For these control loops to operate, they require feedback of their respective controlled variables. This feedback is generally provided by sensors on-board the quadcopter\footnote{Whilst systems have been demonstrated that make use of external position sensors, such as cameras, such systems are not suitable for general use in unstructured environments and shall not be discussed here.}. Some of the most common sensors, and the measurements they provide, are:

\begin{description}
	\item[Linear Accelerometers]\hfill\\
		Linear accelerometers provide a measurement, in standard units, of acceleration along a defined axis. Accelerometers used on quadcopters are often of the MEMS type, as they are small, inexpensive, robust and have low power requirements. MEMS accelerometers are often available in a package which contains three accelerometers aligned to orthogonal \(x\), \(y\) and \(z\) axes.
	\item[Rate Gyroscopes]\hfill\\
		Rate gyroscopes provide a measurement, in standard units, of the angular velocity around a defined axis. Similarly to linear accelerometers, MEMS units are available which may include multiple gyroscopes for \(x\), \(y\) and \(z\) axes.
	\item[Magnetometers]\hfill\\
		Magnetometers can provide a three-dimensional magnetic field vector indicating the direction of magnetic north. This may be used to provide the quadcopter with an absolute heading reference. Magnetometers are available in compact, low-power integrated circuit packages.
	\item[Barometers]\hfill\\
		Barometers, or air pressure sensors, may be used to provide the quadcopter with an absolute altitude measurement. Such sensors are subject to transient changes in atmospheric conditions and may not offer an accurate altitude measurement under all conditions.
	\item[GPS]\hfill\\
		GPS receivers can provide the quadcopter with an absolute position reference, i.e. one that does not drift over time. GPS receivers do have some drawbacks; their resolution and accuracy are limited, their update rate is relatively low, and they require a line-of-sight path to the GPS satellite constellation.
\end{description}

It is important to note that not every measurement required by the PID controllers is available from these sensors. In particular, there is no sensor for transational velocity or angular position, two important variables for maintaining stability.

To circumvent this limitation, it is possible to fuse data from multiple sensors to provide an estimate of these variables. Take, for example, the estimation of pitch and roll angles. One indirect measurement of these variables can be obtained by integrating the angular velocity output of a rate gyroscope. This can provide accurate information during short-term, high-amplitude disturbances, but suffers from drift over time.

On the other hand, since linear accelerometers respond to gravitational acceleration, an orthogonal triad of accelerometers can indicate the current orientation of the vehicle relative to the gravity vector. However, this measurement becomes masked by the acceleration of the quadcopter during agressive maneuvers.

By combining these two measurements, it is possible to produce a single estimate of the pitch and roll angles that does not suffer from either drawback. During periods of high acceleration, the gyroscope measurement is favoured due to its greater short-term accuracy. In the long term, the accelerometer measurement prevents the pitch and yaw angles from drifting.

Other variables can be estimated in a similar manner. For example, a short-term position estimate can be obtained from the second integral of linear accelerometer measurements, then combined with a GPS position measurement. This produces a single position measurement that is both responsive in the short term and does not drift over time.

One generic framework for performing this kind of sensor fusion is the \textit{Kalman Filter}\cite{kalman1960}. The Kalman filter can be used to combine time-series measurements from multiple sensors into a single estimate of their common measured variable. The resulting estimate is more accurate than that from any sensor alone.

Kalman filters see extensive use in a vast range of measurement and control systems, and have become an staple of applied control theory. Various modified Kalman filters have been developed, generally with the aim of improving their performance in non-linear systems. The Extended Kalman Filter (EKF) and the Unscented Kalman Filter (UKF) are two such modifications which are commonly found in practical systems.

\section{Computer Vision}
\label{sec:computerVision}
This section presents some basic concepts in computer vision, in addition to some of the specific algorithms and techniques used in this project.

\subsection{Images as Matrices}
The representation of images as matrices is at the core of many computer vision techniques. In this representation, the rows and columns of the image correspond with the rows and columns of a matrix. Each element of the matrix represents the scalar brightness of the pixel at its corresponding location. For colour spaces other than greyscale, separate matrices are generally constructed, each containing one scalar value pertaining to the colour space. For example, an RGB image would be represented by seperate red, green and blue matrices, each with the same number of rows and columns as the source image.

This matrix abstraction allows standard linear algebra techniques to be applied to images. For example, images can be added and subtracted, or multiplied by a scalar to change their contrast. If a scalar is added to all elements, this results in a perceived change in brightness.

More complex operations also become available. For example, convolution with a kernel can be used to implement sharpen and blur operations, or enhance edges. Spatial Fourier transforms may also be used to convert the image into a complex spatial frequency representation, which can provide information about the tilt and composition of the image.

\subsubsection{Image Pyramids}
Many computer vision algorithms are designed to operate on multiple scales. This allows, for example, a tracking algorithm to continue tracking an object as it moves towards and away from the camera. To improve the ability of algorithms to operate on multiple scales, a common technique is the representation of images in what are known as `pyramids' - that is, multiple copies of an image are provided in different resolutions. The `pyramid' analogy refers to how such copies are generated.

Consider the original image as a plane in three dimensional space. Next, consider a point located above the centre of this plane. Four lines may be drawn from this point to intersect the four corners of the image. These four lines form a pyramid in space. If the image is moved towards or away from the first point, whilst keeping its corners in place along the four lines, the image will appear to shrink as it moves towards the point and stretch as it moves away. By integrating this process, a `solid' three-dimensional pyramid may be created which represents the image at every possible scale. Taking a cross-section of this pyramid, parallel to the original image, provides a scaled copy of the image.

The advantage of representing features in different resolutions is that as a given feature changes scale within the image, it will be represented by a greater or lesser number of pixels. Interpolating or extrapolating the original image can increase or reduce the number of pixels describing a feature, bringing its representation closer to that prior to scaling.

\subsection{Features and Feature Quality}
\label{section:features}
Various computer vision algorithms rely on the ability to detect `features' in images. The exact definition of a feature will vary depending on context, but some feature types are common to many algorithms. Examples of such features are corners, edges and blobs.

As well as detecting different kinds of features, algorithms exist that detect the same features but using different methods. This may result in a different set of features being selected, which will often have slightly different properties depending on the algorithm used to detect them.

A common theme in feature detection algorithms is the use of image gradients. An image gradient is defined as the rate of change of an image quantity with distance, in a particular direction across the image. The image quantity is often brightness, though it may be hue, saturation, luminance, or any other pixel-wise quantity used to represent the image.

Image gradients are important to feature detection because they can help identify areas where the image changes rapidly from light to dark, or vice versa. In practice, such regions often correspond to edges of objects in the scene. Corners of objects may present strong gradients in two directions.

Corners are considered to be high quality features for algorithms which involve tracking objects, because their multiple gradients constrain the feature location in more than one axis. In contrast, edges tend to have a high degree of self-similarity along their length, making it difficult to fixate on a constant point along the edge. This can result in the feature `sliding' along the edge, giving the impression of motion where there may be none. 

For tracking purposes, it is desirable to rank features by some measure which indicates how likely they are to be successfully tracked. Given that feature tracking can be a computationally expensive operation, it is logical to expend resources on tracking the features which provide the best results (longest lifetime, least jitter). There exist various methods for ranking features. Often these ranking methods are tightly coupled to the algorithms used to detect features, such that the feature detection algorithm will return a set of features ranked according to its internal quality measure. Potential quality measures include the amplitude of an image derivative, or the eigenvalues of a derivative matrix surrounding the feature.

In addition to the concept of image features as discrete points, there also exists the concept of an \textit{image descriptor}. An image descriptor is a mathematical construct designed to represent visual information in an invariant manner. Image descriptors are often invariant with scale, rotation and translation. The value of an image descriptor lies in the ability to identify visually similar components from different images, where the component may be moved, rotated, partially obscured or scaled.

For example, an image descriptor may be formed using a source image of an apple. This descriptor may then be used to identify visually similar apples within other images.

\subsection{Optical Flow}
% Define optical flow
Optical flow algorithms are an important class of algorithms in computer vision. Such algorithms aim to infer the motion of objects in the field of view of a camera, or equivalently, infer the motion of the camera relative to a static scene. These algorithms operate on video streams, or a series of consecutive images, tracking objects from one frame to the next.

The output of an optical flow algorithm is a vector, or series of vectors, each describing the direction and distance that a given feature (or the entire scene) has moved between two images.

Different techniques exist for performing optical flow calculations. The two main classes of optical flow algorithms are dense optical flow and sparse optical flow.

It is worth noting that a third technique exists for inferring motion from vision. This technique uses feature descriptors as discussed in section \ref{section:features}. In this technique, feature descriptors are found for objects in a source image, then the target image is searched for matching descriptors. The positions of the feature in both images can be compared, with the difference providing a measure of motion. An advantage of the descriptor-based method is the ability to recover previously-identified features which have been lost or moved out of frame. However, the significant computational complexity of this method meant that it was not suitable for this project.

\subsubsection{Dense Optical Flow}
Dense optical flow calculations operate on the entire image in a pixel-by-pixel fashion. For each pixel in a first image, the optical flow algorithm will attempt to locate the pixel in a second image. The result of this calculation is a vector field describing the changes across the whole scene.

\subsubsection{Sparse Optical Flow}
Sparse optical flow attempts to track a limited set of features from one frame to the next. Features, such as corners, are chosen which have a high likelihood of being located in subsequent images.

Sparse optical flow algorithms are generally faster to execute, as they have to track fewer objects than dense optical flow algorithms. This benefit may be offset by the decreased amount of information provided by a sparse optical flow algorithm, which may limit the suitability of such algorithms for certain purposes.

\subsubsection{The Kanade-Lucas-Tomasi Algorithm}
The Kanade-Lucas-Tomasi (KLT) algorithm implements sparse optical flow. The algorithm works by attempting to align small image patches, using image gradients to direct the iterative alignment process. The KLT algorithm was used as the feature tracker in this project.