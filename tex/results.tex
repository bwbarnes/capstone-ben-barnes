\chapter{Results}
\label{chapter:results}

\section{Preliminary Experiments}

\subsection{Feature Detection Time}

The results for the feature detection time tests are shown in figure \ref{fig:detector_speed_all}. A detailed plot for the AST-based detectors is shown in figure \ref{fig:detectors_ast}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{img/fig_detector_speed_all}
	\caption[Detector time vs. number of features] {
		Detector time vs. number of features.
	}
	\label{fig:detector_speed_all}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{img/fig_detectors_ast}
	\caption[AST detector time vs. number of features] {
		AST detector time vs. number of features (detail).
	}
	\label{fig:detectors_ast}
\end{figure}

\FloatBarrier

It is immediately apparent from these figures that the detection time for all algorithms is almost constant, regardless of the number of features detected. The detailed plot for the AST detectors shows some dependency on input size, particularly for the AGAST detector.

The reason for the near-constant execution time is that regardless of the number of features required, each algorithm iterates over the entire image in the same manner. The algorithm then returns only the requested number of features as a subset of those that it detects. The correlation between the execution time and the requested feature number may be due to the formatting of data for return by the algorithm, such as constructing feature objects.

\subsection{KLT Tracking Time}

The results for the feature tracking timing tests can be seen in figure \ref{fig:klt_timing}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{img/fig_klt_timing}
	\caption[KLT tracking time vs. number of features] {
		KLT tracking time vs. number of features.
	}
	\label{fig:klt_timing}
\end{figure}

The data from this test shows a linear relationship between tracking time and the number of tracked features. This matches the expected behaviour of the algorithm, which should take an approximately constant time per feature on average; at least, there is no correlation between the feature's index in the set and the time taken to track it.

\FloatBarrier

\subsection{Feature Lifetime}

Two video sequences were used to perform the feature lifetime tests. The first was captured from a consumer-grade quadcopter, the ARDrone 1.0, in a closed indoor environment. This sequence had a resolution of 320x240 pixels. The second sequence was captured from the Firefly camera used elswewhere in the project, and had a resolution of 640x480 pixels. The results of the feature lifetime tests can be seen in figures \ref{fig:point_life_ardrone} and \ref{fig:point_life_firefly}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{img/fig_point_life_ardrone}
	\caption[Feature count vs. frame number, sequence 1] {
		Feature count vs. frame number, ARDrone test sequence
	}
	\label{fig:point_life_ardrone}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{img/fig_point_life_firefly}
	\caption[Feature count vs. frame number, sequence 2] {
		Feature count vs. frame number, Firefly test sequence
	}
	\label{fig:point_life_firefly}
\end{figure}

The results from these tests show some discrimination between feature detectors based on lifetime. The ARDrone test sequence shows that Harris and GFTT performed best, giving similar results. It was expected that GFTT would perform well in this test, given that it was designed specifically for the KLT algorithm.

Of the AST-based detectors, FAST appeared to show the best performance on average. It was followed closely by AGAST, with the standalone AGAST implementation consistently giving the worst performance.

The data from the Firefly test sequence may be slightly misleading as the initial number of features differed for the different detectors. This was due to the AST-based algorithms being unable to detect the correct number of features from the first video frame.

Taking this into account, it is possible to draw some conclusions from this data. Once again, Harris and GFTT showed the best results, though after the gap in initial features is considered, FAST performs similarly. The difference between FAST and AGAST is of particular note - despite starting with almost the same number of features, the separation between their plots grows rapidly. The standalone AGAST implementation appears to perform better, on average, than AGAST in this case.

\FloatBarrier

\subsection{USB Latency Tests}

As of the present time, the USB latency tests are undergoing further work. Preliminary results from these tests suggest that a combined delay of approximately 40 ms occurs between triggering the camera's shutter and receiving the image. This delay varies depending on the frame rate, shutter speed and image resolution. One current area of work is determining the different factors contributing to this delay such as exposure time, image processing time, USB polling and transfer delays, and operating system delays.

\section{Final Application Performance}

The results for the final application were obtained by executing the application on-board the Odroid U3, operating on a simulated data set that provided realistic visual and inertial data. The Odroid was required to perform all operations that would be needed in a real-time impmlementation, with the exception of obtaining IMU and vision data from external sensors. These operations included feature extraction, feature management, feature tracking and visual/inertial fusion using a Kalman Filter. For a description of the application's architecture, see section \ref{sec:topLevelLayout}.

Approximate times for the more complex operations are listed in table \ref{tab:applicationTiming}.

\begin{table}[h]
	\centering
	\begin{tabular}{|l|c|}
		\hline
		Operation & Duration\\
		\hline
		Kalman filter, inertial update & 1 ms\\
		Kalman filter, visual update (per feature pair) & 0.5 ms\\
		KLT, 40 features & 5 ms\\
		Point management, 20 features added & 5 ms\\
		\hline
	\end{tabular}
	\caption[Final application timing breakdown]{Final application timing breakdown.}
	\label{tab:applicationTiming}
\end{table}

As an example of how these operations could be scheduled in a real-time system, see figure \ref{fig:applicationSchedulingExample} (figure prepared by Dinuka Abeywardena). The abbreviations used in this figure are defined in table \ref{tab:applicationTimingAbbreviations}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{img/applicationSchedulingExample}
	\caption[Application scheduling example] {
		An example of how the different operations and deadlines may be arranged in a real-time application.
	}
	\label{fig:applicationSchedulingExample}
\end{figure}

\begin{table}[h]
	\centering
	\begin{tabular}{|l|l|}
		\hline
		Abbreviation & Definition\\
		\hline
		I & Inertial measurement arrives\\
		V & Visual measurement arrives\\
		KFI & Kalman filter update, inertial\\
		FT & Feature tracking\\
		PM & Point management\\
		KFV & Kalman filter update, visual\\
		\hline
	\end{tabular}
	\caption[Application scheduling abbreviations]{Abbreviations used in the application scheduling example.}
	\label{tab:applicationTimingAbbreviations}
\end{table}

As the data set had been created from a simulation, ground truth data was provided from the simulation program. Additionally, a MATLAB reference implementation was provided by Dinuka Abeywardena, the author of the Kalman filter software used in this application. This allowed the results of the tracking to be compared to both the ground truth data and the MATLAB implementation. The results of the Odroid application were seen to be equivalent to the MATLAB implementation.

These timing results show that a real-time implementation of the application is possible, with visual updates at 50 Hz and inertial updates at 200 Hz.

\FloatBarrier

\section{Conclusions}
The results of both the preliminary experimentation and the final application performance demonstrate that visual/inertial fusion can be performed in real-time on-board a quadcopter using low-cost hardware. Such fusion can be achieved at a vision update rate of 50 Hz and an IMU update rate of 200 Hz. Examination of the processor utilisation with standard system management tools showed that the utilisation of all four of the Odroid's cores rose to around 90\% during the execution of this application, indicating that the application's multithreaded design allowed for efficent use of the available computing resources. A comparison of the application output with a MATLAB reference implementation showed that the quality of the state estimates did not suffer due to the implementation of this visual-inertial fusion method on-board the Odroid.

\section{Future Work}
Much of the future work for this project involves implementing the system on-board a quadcopter and analysing its performance under real-world conditions. This will involve interfacing the Odroid with the required sensors and low-level quadcopter control electronics. Preliminary proposals for such interfacing involve the use of a commercial controller to perform the low-level stabilisation routines, which will communicate with the Odroid via I\textsuperscript{2}C. The commercial controller will also provide IMU measurements to the Odroid.